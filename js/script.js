
let arr1 = ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];

let listNode = document.querySelector('.list-node');

 function arrayToList(arr1, node = document.body) {
     for(let i = 0; i < arr1.length; i++)
     {
         listNode.innerHTML += `<div >${arr1[i]}</div>`;
     }
 }
arrayToList(arr1, listNode);

// 1.Опишите, как создать новый HTML тег на странице.
//     DOM-узел можно создать двумя методами:
//     document.createElement(tag)
// Создаёт новый элемент с заданным тегом:
//     let div = document.createElement('div');
// document.createTextNode(text)
// Создаёт новый текстовый узел с заданным текстом:
//     let textNode = document.createTextNode('А вот и я');
//
// 2.    Опишите, что означает первый параметр функции insertAdjacentHTML, и опишите возможные варианты этого параметра.
//     elem.insertAdjacentHTML(where, html)
//     Первый параметр – это специальное слово, указывающее, куда по отношению к elem производить вставку.
//     Значение должно быть одним из следующих:
//     "beforebegin" – вставить html непосредственно перед elem,
//     "afterbegin" – вставить html в начало elem,
//     "beforeend" – вставить html в конец elem,
//     "afterend" – вставить html непосредственно после elem.
//     Второй параметр – это HTML-строка, которая будет вставлена именно «как HTML».
//
// 3.    Как удалить элемент со страницы?
//     Для удаления узла есть методы node.remove().